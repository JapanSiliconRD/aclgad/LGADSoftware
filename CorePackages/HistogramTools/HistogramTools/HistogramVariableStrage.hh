#ifndef __HistogramVariableStrage_HH__
#define __HistogramVariableStrage_HH__

#include <iostream>
#include <string>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"



class HistogramVariableStrage{
private:
  std::vector<std::string> histname;
  std::vector<std::string> hist2Dname;
  std::vector<std::string> hist3Dname;
  std::vector<TH1D*> histstrage;
  std::vector<TH2D*> hist2Dstrage;
  std::vector<TH3D*> hist3Dstrage;
  
  std::string suffix;
public:
  HistogramVariableStrage(std::string _suffix){
    histname.clear(); histstrage.clear(); suffix=_suffix;
  }
  void PrintHistName();
  std::vector<std::string> GetHistName(){return histname;}
  std::vector<std::string> Get2DHistName(){return hist2Dname;}
  std::vector<std::string> Get3DHistName(){return hist3Dname;}
  void Add4vecHistogram(std::string name,int nbins,double xmin,double xmax,int mnbins=200,double mxmin=0,double mxmax=200);
  void AddHistogram(std::string name,int nbins,double xmin,double xmax,double ymin=0,double ymax=-1);
  void Add2DHistogram(std::string name,int nxbins,double xmin,double xmax,int nybins,double ymin,double ymax);
  void Add3DHistogram(std::string name,int nxbins,double xmin,double xmax,int nybins,double ymin,double ymax,int nzbins,double zmin,double zmax);
  TH1D* GetHistogram(std::string name);
  TH2D* Get2DHistogram(std::string name);
  TH3D* Get3DHistogram(std::string name);
  std::vector<TH1D*> Get4vecHistogram(std::string name);
};



#endif
