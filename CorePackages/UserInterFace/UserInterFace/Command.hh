#ifndef COMMAND_HH
#define COMMAND_HH

#include <iostream>
#include <string>
#include <vector>

class Command
{
private:
  std::vector<std::string> m_infoList; // List of arguments for one command
public:
  Command() {};
  ~Command() {};
  std::vector<std::string> getCmdInfo(const std::string &str);
};

#endif // #ifndef COMMAND_HH
