cmake_minimum_required(VERSION 3.1)
project(LGADanalysis LANGUAGES CXX)

# Finding the ROOT package
find_package(ROOT 6.16 CONFIG REQUIRED)
# Sets up global settings
include("${ROOT_USE_FILE}")

include(cmake/CMakeLists.txt.CorePackages)
include(cmake/CMakeLists.txt.LGADTools)


# for PulseHeight Pacakge
#include_directories(PulseHeight/PulseHeight)
#add_subdirectory(PulseHeight)
#set (EXTRA_LIBS ${EXTRA_LIBS} PulseHeight)
#INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/PulseHeight/libPulseHeight.so DESTINATION ${PROJECT_SOURCE_DIR}/lib)


message(STATUS, "message : ${EXTRA_LIBS}")


#foreach( ILIBS ${EXTRA_LIBS})
#	 INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/${ILIBS}/lib${ILIBS}.so DESTINATION ${PROJECT_SOURCE_DIR}/lib)
#endforeach( ILIBS ${EXTRA_LIBS})



# Adding an executable program and linking to needed ROOT libraries
add_executable(${PROJECT_NAME} ${PROJECT_NAME}/${PROJECT_NAME}.C)
target_link_libraries(${PROJECT_NAME} PUBLIC 
	 		  ${EXTRA_LIBS}
		          ${ROOT_LIBRARIES}
			  ${ROOT_EXE_LINKER_FLAGS})


set(TARGET_INSTALL_AREA ${PROJECT_SOURCE_DIR})
set(SHARED_INSTALL_AREA ${PROJECT_SOURCE_DIR}/installed/)
add_compile_options(-std=c++11)



INSTALL(TARGETS ${PROJECT_NAME}
        EXPORT ${PROJECT_NAME}
        RUNTIME DESTINATION ${PROJECT_SOURCE_DIR}/bin
        LIBRARY DESTINATION ${PROJECT_SOURCE_DIR}/lib
        ARCHIVE DESTINATION ${PROJECT_SOURCE_DIR}/lib)
