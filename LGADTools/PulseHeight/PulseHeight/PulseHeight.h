#ifndef __PulseHeight_h__
#define __PulseHeight_h__
#include <iostream>
#include "TGraphErrors.h"
#include "TF1.h"

class PulseHeight{
private : 
  double ADCcount[1024];
  double ADCcounte[1024];
  double Time[1024];
  double Timee[1024];
  TGraphErrors *gr;
  TF1 * fitfunc;
  double minadc;
  double pulseheight;
  double baselineadc;
  double timeminadc;
  double timearrival;
  double time100mV;
  double timeoffset;
  double adcscale;
  bool havesetTimeADC;
  double FitChisq;
  double FitPar[5];
  double FitPulseHeight;
  TFitResultPtr r;
 public:
  PulseHeight();
  PulseHeight(double *time,double *adc,double *timee, double *adce);
  ~PulseHeight(){}
  double GetMinADC(){return minadc;}
  double GetTimeMinADC(){return timeminadc;}
  double GetTimeArrival(){return timearrival;}
  double GetTime100mV(){return time100mV;}
  double GetBaseLineADC(){return baselineadc;}
  double GetPulseHeight(int type=1);
  double GetIntegral(int type);
  double GetFitChisq(){return FitChisq;}
  double*GetFitPar(){return FitPar;}
  double GetFitResultPtr(){return r;}

  void SetTimeADC(double *time,double *adc,double *timee, double *adce);
  void SetOffset(double to, double as=1);
  double FitGraph(bool fordisplay=false);
  void MakeGraph();
  TGraphErrors * GetGraph(){ return gr; }
  TF1 * GetFitfunc(){ return fitfunc; }
  void PrintADCcount();
  void GetMinADC(double &mintime, double &minADC);
  void GetArrivalTime(double &arrivaltime);
};
#endif
