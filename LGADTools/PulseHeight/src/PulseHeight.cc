#include "PulseHeight.h"


PulseHeight::PulseHeight(){
  timeoffset=0;
  adcscale=1;
  FitPulseHeight=0;
}
PulseHeight::PulseHeight(double *time,double *adc,double *timee, double *adce){
  timeoffset=0;
  adcscale=1;
  SetTimeADC(time,adc,timee,adce);
  havesetTimeADC=false;
}


double PulseHeight::GetPulseHeight(int type){
  if(type==1)return baselineadc-minadc;
  if(type==2)return GetIntegral(0);
  if(type==3)return GetIntegral(1);
  if(type==4)return FitPulseHeight;
  return 0;
}
double PulseHeight::GetIntegral(int type){
  double neg=0,pos=0,all=0;
  for(int ii=0;ii<1023;ii++){
    if(ADCcount[ii]<0){
      neg+=-1*ADCcount[ii];
    }else{
      pos+=ADCcount[ii];
    }
    all+=-1*ADCcount[ii];
  }
  if(type==-1){
    return neg/200;
  }else if(type==0){
    return all/200;
  }else if(type==1){
    return pos/200;
  }else{
    std::cout << "please use type = -1, 0, 1" << std::endl;
    return -1;
  }
  
}
void PulseHeight::SetTimeADC(double *time,double *adc,double *timee, double *adce){
  for(int ii=0;ii<1023;ii++){
    ADCcount[ii]=adc[ii];
    ADCcounte[ii]=adce[ii];
    Time[ii]=time[ii];
    Timee[ii]=timee[ii];
  }

  double baseline=0;
  for(int ii=0;ii<1023;ii++){
    if(ii<10)baseline+=ADCcount[ii];
    if(ii==10){
      baseline/=10.;
      baselineadc=baseline;
    }
  }
  for(int ii=0;ii<1023;ii++){
    ADCcount[ii]-=baselineadc;
  }
  baselineadc=0;
  havesetTimeADC=true;
}
void PulseHeight::SetOffset(double to, double as){
  if(!havesetTimeADC){
    std::cout << "please set Time and ADC first" << std::endl;
    return;
  }
  timeoffset=to;
  adcscale=as;
  for(int ii=0;ii<1023;ii++){
    ADCcount[ii]*=adcscale;
    Time[ii]+=timeoffset;
  }
  
}
void PulseHeight::MakeGraph(){
  gr=new TGraphErrors(1023,Time,ADCcount,Timee,ADCcounte);
}
double PulseHeight::FitGraph(bool fordisplay){
  fitfunc = new TF1("wavefunc","([0]*TMath::Erf((x-[1])/[2])+[0])*([3]*exp(-1*(x-[4])/[5]))",-1,1.3);
  fitfunc->SetParameter(0,-1);
  fitfunc->SetParameter(1,0.5);
  fitfunc->SetParameter(2,0.5);
  fitfunc->SetParameter(3,0.3);
  fitfunc->SetParameter(4,1);
  fitfunc->SetParameter(5,0.5);
  r = gr->Fit(fitfunc,"Q","",-1,1.3);
  //  std::cout << r << " " << fitfunc->GetChisquare()/fitfunc->GetNDF()<< std::endl;
  fitfunc->GetParameters(FitPar);
  FitChisq=fitfunc->GetChisquare()/fitfunc->GetNDF();
  FitPulseHeight=-1*fitfunc->GetMinimum(-1,1.3);
  if(!fordisplay){
    delete gr;
    delete fitfunc;
  }
  return FitChisq;
}
void PulseHeight::PrintADCcount(){
  std::cout << "adc = " ;
  for(int ii=0;ii<1023;ii++)std::cout << ADCcount[ii] << " " ;
  std::cout << std::endl;
}
void PulseHeight::GetMinADC(double &mintime, double &minADC){
  //    double mintime=0;
  //    double minADC=1;
  bool findmin=false;
  for(int ii=0;ii<1023;ii++){
    if(minADC>ADCcount[ii]){
      minADC=ADCcount[ii];
      mintime=Time[ii];
      findmin=true;
    }
    
  }
  minadc=minADC;
  timeminadc=mintime;
  //    if(mintime<1000)PrintADCcount();
  //    return mintime;
}
void PulseHeight::GetArrivalTime(double &arrivaltime){
  double mintime=0;
  double minADC=1;
  GetMinADC(mintime, minADC);
  //    std::cout << mintime << " " << minADC << std::endl;
  bool is100mV=false;
  bool ishph=false;
  double baseline=0;
  for(int ii=0;ii<1023;ii++){
    if(ii==0)continue;
    if(ii>10){
      double a1=ADCcount[ii-1];
      double a2=ADCcount[ii];
      double t1=Time[ii-1];
      double t2=Time[ii];
      double H2=baseline-(baseline-minADC)/2.;
      if(ADCcount[ii]<baseline-(baseline-minADC)/2.&&!ishph){
	arrivaltime=(t2-t1)*(H2-a1)/(a2-a1)+t1;
	ishph=true;
      }
      if(ADCcount[ii]<baseline-0.1 && !is100mV){
	time100mV=(t2-t1)*(H2-a1)/(a2-a1)+t1;
	is100mV=true;
      }
    }
  }
  timearrival=arrivaltime;
}
