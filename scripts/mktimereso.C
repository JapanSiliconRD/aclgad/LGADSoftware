TFile *fp;
TH1D* GetHistogram(std::string str){
  return (TH1D*)fp->Get(str.c_str());
}
TH2D* Get2DHistogram(std::string str){
  return (TH2D*)fp->Get(str.c_str());
}

void mktimereso(std::string fname="../results/plots_pad_stack_scinti_C2_200V.root", std::string sufix="pad_stack_scinti_C2_200V_run2"){
  fp = new TFile(fname.c_str());

  std::vector<int> layerlist;
  layerlist.push_back(0);
  layerlist.push_back(1);
  
  std::vector<int> numchinlayer;
  numchinlayer.push_back(4);
  numchinlayer.push_back(4);
 
  bool is_Strip =false;
  bool is_Pad = true;


  TCanvas *c3 = new TCanvas("c3","c3",2000,1000);
  c3->Divide(4,2);
  c3->cd(1);
  if(layerlist.size()==2){
    TF1* ga=new TF1("ga","gaus");
    GetHistogram("layertimediff_")->Fit("ga","Q","");
    GetHistogram("layertimediff_")->Draw();
    std::cout << "total" << " : " << ga->GetParameter(2) << "+-" << ga->GetParError(2) << std::endl;
    int kk=0;
    for(int ii=0;ii<numchinlayer[0];ii++){
      for(int jj=0;jj<numchinlayer[1];jj++){
	std::stringstream ss; ss.str("");
	ss<< "layertimediff" << ii << jj << "_";
	TF1* ga=new TF1(ss.str().c_str(),"gaus");
	ga->SetParameter(1,0);
	GetHistogram(ss.str())->SetMarkerColor(kk+2);
	GetHistogram(ss.str())->SetLineColor(kk+2);
	GetHistogram(ss.str())->Draw("Same");
	GetHistogram(ss.str())->Fit(ss.str().c_str(),"Q","");
	ga->Draw("Same");
	std::cout << ss.str() << " : " << ga->GetParameter(2) << "+-" << ga->GetParError(2) << std::endl;;
	kk++;
      }
    }
    c3->Print(("results/timereso_"+sufix+".png").c_str());
    c3->Print(("results/timereso_"+sufix+".pdf").c_str());
  } 
  c3->cd(1);
  Get2DHistogram("lytdiffvsph_")->Draw("COLZ");

  double binsize=0.01;
  double startbin=0;
  int cannum=2;
  std::vector<double> thvolt; thvolt.clear();
  thvolt.push_back(0.05);
  thvolt.push_back(0.06);
  thvolt.push_back(0.07);
  thvolt.push_back(0.08);
  thvolt.push_back(0.09);
  thvolt.push_back(0.10);
		
  std::vector<TH2D*> h2dlist; h2dlist.clear();
  for(int ii=0;ii<numchinlayer[0];ii++){
    for(int jj=0;jj<numchinlayer[1];jj++){
      std::stringstream ss; ss.str("");
      ss<< "lytdiffvsph" << ii << jj << "_";
      TH2D * h2d = Get2DHistogram(ss.str());
      h2dlist.push_back(h2d);
    }
  }

  std::vector<double> timereso;
  std::vector<double> timeresoe;
  std::vector<double> zero;
  
  for(auto th: thvolt){
    zero.push_back(0.);
    std::cout << "====== threshold " << th*1000 << "mV  ====="<< std::endl;
    c3->cd(cannum++);
    TF1* ga=new TF1("ga","gaus");
    std::stringstream ss; ss.str("");
    ss<< th*1000 << "mV";
    TH1D* h = Get2DHistogram("lytdiffvsph_")->ProjectionX(ss.str().c_str(),(th-startbin)/binsize +1,-1);
    h->Fit("ga","Q","");
    h->GetXaxis()->SetRangeUser(-0.5,0.5);
    h->Draw();
    std::cout << "total" << " : " << ga->GetParameter(2) << "+-" << ga->GetParError(2) << std::endl;


    int kk=0;
    double nume=0;
    double deno=0;
    for(int ii=0;ii<numchinlayer[0];ii++){
      for(int jj=0;jj<numchinlayer[1];jj++){
	std::stringstream ss0; ss0.str("");
	ss0<< "lytdiffvsph" << ii << jj << "_";
	//	TH2D * h2d = Get2DHistogram(ss0.str());
	TF1* ga=new TF1(ss0.str().c_str(),"gaus");
	ga->SetParameter(1,0);
	TH1D *h0 = h2dlist[kk]->ProjectionX((ss0.str()+ss.str()).c_str(),(th-startbin)/binsize +1,-1);
	h0->SetMarkerColor(100-kk*3);
	h0->SetLineColor(100-kk*3);
	h0->Draw("Same");
	TFitResultPtr r =h0->Fit(ss0.str().c_str(),"Q","");
	ga->SetLineColor(100-kk*3);
	ga->Draw("Same");
	std::cout << ss0.str() << " : " << ga->GetParameter(2) << "+-" << ga->GetParError(2) << std::endl;;
	if(r==0){
	  nume+= ga->GetParameter(2)/ga->GetParError(2)/ga->GetParError(2);
	  deno+= 1/ga->GetParError(2)/ga->GetParError(2);
	}else{
	  std::cout << "Fit failed !!  skipped " <<  std::endl;
	}
	kk++;
      }
    }
    std::cout << "Corrected sigma(T2-T1)     : " << nume/deno << "+-" << 1./sqrt(deno) << std::endl;
    std::cout << "Corrected time resolution  : " << nume/deno/sqrt(2) << "+-" << 1./sqrt(deno)/sqrt(2) << std::endl;
    timereso.push_back(nume/deno/sqrt(2));
    timeresoe.push_back(1./sqrt(deno)/sqrt(2));
    
  }
  c3->cd(cannum);
  TH2D *frame = new TH2D("frame","frame",1,0,0.12,1,0,0.07);
  frame->GetXaxis()->SetTitle("Threshold voltage [V]");
  frame->GetYaxis()->SetTitle("TimeResolution [ns]");
  TGraphErrors *gr = new TGraphErrors(thvolt.size(),&thvolt[0], &timereso[0], &zero[0], &timeresoe[0]);
  frame->Draw();
  gr->Draw("PSame");
  c3->Update();






  return;
}
